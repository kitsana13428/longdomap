function init() {
    var marker1 = new longdo.Marker({ lon: 100.56, lat: 13.60 },
        {
          title: 'Marker',
          icon: {
            url: 'https://map.longdo.com/mmmap/images/pin_mark.png',
            offset: { x: 12, y: 45 }
          },
          detail: 'Drag me',
          visibleRange: { min: 6, max: 12 },
          draggable: true,
          weight: longdo.OverlayWeight.Top,
        });
        
    var marker = new longdo.Marker({ lon: 100.56, lat: 13.74 });
    var map = new longdo.Map({
      placeholder: document.getElementById('map'),
      
    });
    map.Layers.setBase(longdo.Layers.OSM);
    map.Overlays.add(marker);
    map.Overlays.add(marker1);
  }

  